# Data set read- we don't want R to transfer automaticly the srting to factors.
spam<- read.csv('spam.csv', stringsAsFactors = FALSE )

install.packages('tm')
library(tm)

install.packages('wordcloud')
library(wordcloud)

install.packages('e1071')
library(e1071)
