iris

library(ggplot2)
p1 <- ggplot(iris, aes(Petal.Length,Petal.Width, color= Species))
p1 <- p1 + geom_point(size=4)

?kmeans

#set of random numbers
set.seed(101)

str(iris)


irisClaster <- kmeans(iris[,1:4],3, nstart = 20)

irisClaster$cluster

table(irisClaster$cluster, iris$Species)

install.packages('cluster')
library(cluster)

clusplot(iris, irisClaster$cluster, color =T, shade =T,labels = 0,lines =0)

