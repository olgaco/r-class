7
x <- 5;
x[1]

#vectors in R
x<- c(1,3,5,6,7,8)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
class(x)

#dataType in R
y <- as.integer(x)
class(y)
z <- as.character(x)

#subseting and sequesnce

a <-1:3

?remove

remove(x)

b <- 7:16

d <- c(17,22,45,78)
d[3]
d[3:4]
d[c(3:4,3:4)]
f <- d[c(3:4,3:4)]

#names for vectors

salary <- c(800,6000,4030,4000)
names(salary) <- c('Alice','John', 'Jerry', 'Jack')
salary['Alice']
names(salary)

salary

#logical operators

x<- c(4,6,100,600)
z<- x>7

x[z]
x[!z]

#negative indexes

x[-2]

x[c(-2,-3)]

x
rev(x)

#sorting
n<-c(8,9,4,7)
sort(z)
sort(n)
sort(n,decreasing = T)

n
order(n)
n[order(n)]

n[n]

seq(1,4,length=7)


x <- c(1,19,23)

sample(x,10,replace = T)

x<- c(x,NA)
x
mean(x)

sum(x)
sum(x, na.rm =T)

is.na(x)

notNA <- !is.na(x)
x[notNA]

#matrices

y <- matrix(c(4,6,7,9,3,9),nrow=2,ncol=3, byrow=T)
y
?matrix

#subseting matrices
y[1,2]
y[2,]
y[,3]
y[,-2]


v1 <- c(1,2,3,4,5)
v2 <- c(5,6,7,8,9)

mat1 <- rbind(v1,v2)
mat2 <- cbind(v1,v2)

mat1[6]
mat2[6]


brands <- c('Ford', 'Mazda', 'Fiat')
from <- c('US', 'Japan', 'Italy')
rank <- c(3,1,2)


cars <- data.frame(brands, from, rank)

class(cars)
typeof(cars)


cars$brands
cars$from
cars$rank

typeof(cars$rank)

barnds.filter <- cars$brands == 'Ford'

cars[barnds.filter, c(1,3)]

summary(cars)
str(cars)

