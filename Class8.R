# Data set read- we don't want R to transfer automaticly the srting to factors.
spam<- read.csv('spam.csv', stringsAsFactors = FALSE )
str(spam)

install.packages('tm')
library(tm)

install.packages('wordcloud')
library(wordcloud)

install.packages('e1071')
library(e1071)

str(spam)
spam$type<-as.factor(spam$type)

####Bais naive algo####

#1.Genereting DTM


##corpus is list type that we want to create from vector- the first part contain text and the second -meta data.
spam_corpus<-Corpus(VectorSource(spam$text))
str(spam_corpus)
spam_corpus[[1]][[1]]
spam_corpus[[1]][[2]]
spam_corpus[[2]][[1]]
spam_corpus[[2]][[2]]

#2.cleaning the text
#remove all punctions
clean_corpus<- tm_map(spam_corpus,removePunctuation)

#removel double spaces between words
clean_corpus<- tm_map(clean_corpus,stripWhitespace)

clean_corpus[[1]][[1]]

#remove Uper case 
clean_corpus<- tm_map(clean_corpus,content_transformer(tolower))

#remove digits
clean_corpus <- tm_map(clean_corpus,removeNumbers)

# remove stop words
clean_corpus<- tm_map(clean_corpus,removeWords, stopwords())

stopwords()

clean_corpus[[1]][[1]]


#3.Create DTM
dtm <- DocumentTermMatrix(clean_corpus)

dim(dtm)

#4. remove infrequent words
frequent_dtm <- DocumentTermMatrix(clean_corpus, list(dictionary = findFreqTerms(dtm,10)))
dim(frequent_dtm)

#Data visualization
pal<- brewer.pal(9, 'Dark2')
#we want to use only frequent words - appears more then 5 times
wordcloud(clean_corpus, min.freq = 5, random.order = FALSE, colors = pal)


wordcloud(clean_corpus[spam$type == 'spam'], min.freq = 5, random.order = FALSE, colors = pal)

wordcloud(clean_corpus[spam$type == 'ham'], min.freq = 5, random.order = FALSE, colors = pal)


###################spliting to train and test sets########################

#numbers from 0-1 , nurmal destribution
split<- runif(500)

#boolean vector - 70% true, 30% false
split<- split >0.3

#dividing raw data from original data set
train_raw <- spam[split,]
test_raw <- spam[!split,]

#dividing clean corpus
train_corpus <-clean_corpus[split]
test_corpus <-clean_corpus[!split]


#dividing dtm
train_dtm <-frequent_dtm[split,]
test_dtm <-frequent_dtm[!split,]


#convert DTM to yes/no
con_yesno <- function(x){
  x<-ifelse(x>0,1,0)
  x<- factor(x, levels=c(1,0), labels = c('Yes','No'))
}

#margin- apply on rows/columns/both
train <- apply(train_dtm, MARGIN = 1:2,con_yesno)
test <- apply(test_dtm, MARGIN = 1:2,con_yesno)

dim(train)
dim (test)

#converting into a data frame- Bayss algo requires data frame
df_train <- as.data.frame(train)
df_test<- as.data.frame(test)

#Add the type column
df_train$type <- train_raw$type
df_test$type <- test_raw$type

dim(df_train)
#Generating the model using naive bayse

model <- naiveBayes(df_train[,-60], df_train$type)

prediction <- predict(model,df_test[,-60])

## creating confision matrix 
install.packages('SDMTools')
library(SDMTools)

# if spam -> 1, else - >0
conv_10 <- function(x){
  x <- ifelse( x =='spam',1,0)
}

pred01 <- sapply(prediction,conv_10)
actual01 <- sapply(df_test$type,conv_10)

confusion <- confusion.matrix(actual01, pred01)

TP <- confusion[2,2]
FP <- confusion[2,1]
TN <- confusion[1,1]
FN <- confusion[1,2]

## the optimal is that those values are 1. If both are very small- the prediction wasn't good
recall <- TP/(TP+FN)
precision <- TP/(TP+FP)